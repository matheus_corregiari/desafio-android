package br.com.matheus.buscacep;

import android.app.Activity;
import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by matheus on 24/09/2014.
 * Classe responsável por salvar e resgatar os dados salvos no aparelho.
 */
public class LocalCoreDataDAO {

	/**
	 * Método que salva um objeto como arquivo
     *
	 * @param context
	 * @param object Objeto a ser salvo
	 * @param filename Nome do arquivo
	 */
	public static void save( Context context, Object object, String filename ) {
		
		ObjectOutputStream objectOut = null;
		try {

			FileOutputStream fileOut = context.openFileOutput( filename, Activity.MODE_PRIVATE );
			objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(object);
			fileOut.getFD().sync();

		} catch (IOException e) {
            e.printStackTrace();
		} finally {
			if (objectOut != null) {
				try {
					objectOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Método que resgata um objeto
     *
	 * @param context
	 * @param filename nome do arquivo a ser resgatado
	 * @return Objeto salvo referente ao nome do arquivo, se não existir o retorno é null
	 */
	public static Object get(Context context, String filename) {

		ObjectInputStream objectIn = null;
		Object object = null;
		
		if ( filename == null || context == null ){
			return null;
		}
		
		try {

			FileInputStream fileIn = context.getApplicationContext().openFileInput(filename);
			objectIn = new ObjectInputStream(fileIn);
			object = objectIn.readObject();

		} catch (Exception e) {
            e.printStackTrace();
		} finally {
			if (objectIn != null) {
				try {
					objectIn.close();
				} catch (IOException e) {
                    e.printStackTrace();
				}
			}
		}

		return object;
	}

}
