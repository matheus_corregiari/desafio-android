package br.com.matheus.buscacep;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matheus on 24/09/2014.
 */
@EActivity(R.layout.activity_my)
public class MyActivity extends Activity {

    /**
     * Nome do arquivo a ser salvo
     */
    private static final String HISTORY_FILE_NAME = "AUSh90384HV93ufv2b3087as";

    /**
     * Campo de texto usado para inserir o Cep
     */
    @ViewById(R.id.cep_editText)
    EditText cepEditText;

    /**
     * Instância do serviço que vai fazer a comunicação REST para buscar o Cep
     */
    @RestService
    CepService cepService;

    /**
     * Instância do ProgressDialog
     */
    private ProgressDialog progressDialog;

    /**
     * Lista de Enderecos salvos, representa o histórico
     */
    private List<Endereco> enderecos;

    /**
     * Evento de Click que vai iniciar o processo de busca de um cep
     */
    @Click(R.id.buscar_button)
    public void callService(){

        String cep = this.cepEditText.getText().toString();
        cep = cep.replaceAll("[-]", "");
        cep = cep.replaceAll("[_]", "");
        cep = cep.replaceAll("[ ]", "");
        cep = cep.replaceAll("[.]", "");
        if(cep == null || cep.equals("")){
            Toast.makeText(getApplicationContext(), R.string.my_activity_insira_cep_label, Toast.LENGTH_SHORT).show();
            return;
        }

        consultaCEP(cep);
    }

    /**
     * Evento de Click que vai mostrar um Dialog, se não existir nenhum item no histórico, o dialog
     * apresentará uma mensagem, caso contrário, mostrará a lista de buscas
     */
    @Click(R.id.historico_button)
    public void showHistory(){

        if(enderecos == null || enderecos.isEmpty()){
            showAlertDialog(R.string.my_activity_historico, R.string.my_activity_historico_vazio);
            return;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyActivity.this);
        alertDialog.setTitle(R.string.my_activity_historico);
        final ArrayAdapter<Endereco> arrayAdapter = new ArrayAdapter<Endereco>(
                MyActivity.this, R.layout.select_dialog_item);
        arrayAdapter.addAll(enderecos);
        alertDialog.setPositiveButton(R.string.my_activity_ok_label,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showAlertDialog(R.string.my_activity_resultado, arrayAdapter.getItem(which).toString());
                    }
                });
        alertDialog.show();

    }

    /**
     * Depois que as Views foram carregadas, é feita a configuração da mascara no campo de texto
     * e o histórico é carregado
     */
    @AfterViews
    public void setupViews(){
        cepEditText.addTextChangedListener(Utils.insert("##### - ###", cepEditText));
        carregarHistoricoDeEnderecos();
    }

    /**
     * Método que carrega o histórico de buscas
     */
    @SuppressWarnings("unchecked")
    @Background
    public void carregarHistoricoDeEnderecos(){
        enderecos = (List<Endereco>) LocalCoreDataDAO.get(MyActivity.this, HISTORY_FILE_NAME);

        if(enderecos == null){
            enderecos = new ArrayList<>();
        }
    }

    /**
     * Método que salva mais um endereço no historico local
     * @param endereco
     */
    @Background
    public void salvarHistoricoDeEnderecos(Endereco endereco){
        if(enderecos == null){
            enderecos = new ArrayList<>();
        }
        enderecos.add(endereco);
        LocalCoreDataDAO.save(MyActivity.this, enderecos, HISTORY_FILE_NAME);
        carregarHistoricoDeEnderecos();
    }

    /**
     * Método que faz a requisição REST e busca o endereço de acordo com o CEP
     * @param cep
     */
    @Background
    public void consultaCEP(String cep){
        showProgress();
        try {
            if(!Utils.isNetworkAvailable(getApplicationContext())){
                throw new NetworkErrorException("no internet");
            }
            Endereco test = cepService.get(cep);
            if(test == null){
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
            salvarHistoricoDeEnderecos(test);
            showAlertDialog(R.string.my_activity_resultado, test.toString());
        }catch(HttpClientErrorException e){
            e.printStackTrace();
            showAlertDialog(R.string.my_activity_problema, R.string.my_activity_cep_null);
        } catch(NetworkErrorException e) {
            e.printStackTrace();
            showAlertDialog(R.string.my_activity_problema, R.string.my_activity_no_connection);
        }
    }

    /**
     * Método que mostra um AlertDialog
     * @param resTitle
     * @param resMessage
     */
    @UiThread
    public void showAlertDialog(int resTitle, int resMessage){
        hideProgress();
        AlertDialog alertDialog = new AlertDialog.Builder(MyActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT).create();
        alertDialog.setTitle(resTitle);
        alertDialog.setMessage(getText(resMessage));
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
        this.cepEditText.setText("");
    }

    /**
     * Método que mostra um AlertDialog
     * @param resTitle
     * @param message
     */
    @UiThread
    public void showAlertDialog(int resTitle, String message){
        hideProgress();
        AlertDialog alertDialog = new AlertDialog.Builder(MyActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT).create();
        alertDialog.setTitle(resTitle);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.my_activity_ok_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
        this.cepEditText.setText("");
    }

    /**
     * Método que mostra o ProgressDialog
     */
    @UiThread
    public void showProgress(){
        try {
            progressDialog = new ProgressDialog(MyActivity.this);
            progressDialog.setMessage(getText(R.string.my_activity_consultando_informacoes));
            progressDialog.setTitle(R.string.my_activity_consultando);
            progressDialog.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Método que esconde o ProgressDialog
     */
    @UiThread
    public void hideProgress(){
        try {
            if(progressDialog != null && progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
