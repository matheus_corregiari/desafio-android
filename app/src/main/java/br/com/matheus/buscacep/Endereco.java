package br.com.matheus.buscacep;

import java.io.Serializable;

/**
 * Created by matheus on 24/09/2014.
 * Classe que representa um endereco
 */
public class Endereco implements Serializable {

    /**
     * Cep do endereco
     */
    private String cep;

    /**
     * Tipo de Logradouro do endereco
     */
    private String tipoDeLogradouro;

    /**
     * Logradouro do endereco
     */
    private String logradouro;

    /**
     * Bairro do endereco
     */
    private String bairro;

    /**
     * Cidade do endereco
     */
    private String cidade;

    /**
     * Estado do endereco
     */
    private String estado;

    public Endereco() {
        this.cep              = "-";
        this.tipoDeLogradouro = "-";
        this.logradouro       = "-";
        this.bairro           = "-";
        this.cidade           = "-";
        this.estado           = "-";
    }

    public String getCep() {
        return this.cep;
    }

    public void setCep(String cep) {

        if(cep == null || cep.equals("")){
            this.cep = "-";
        }

        if(cep != null && cep.length() > 5 && cep.length() <= 8){
           cep = cep.substring(0, 2) + "." + cep.substring(2, 5) + "-" + cep.substring(5, cep.length());
        }else{
            if(cep != null && cep.length() > 2 && cep.length() <= 5){
                cep = cep.substring(0, 2) + "." + cep.substring(2, cep.length());
            }
        }

        this.cep = cep;
    }

    public String getTipoDeLogradouro() {
        return this.tipoDeLogradouro;
    }

    public void setTipoDeLogradouro(String tipoDeLogradouro) {

        if(tipoDeLogradouro == null || tipoDeLogradouro.equals("")){
            this.tipoDeLogradouro = "-";
        }

        this.tipoDeLogradouro = tipoDeLogradouro;
    }

    public String getLogradouro() {
        return this.logradouro;
    }

    public void setLogradouro(String logradouro) {

        if(logradouro == null || logradouro.equals("")){
            this.logradouro = "-";
        }

        this.logradouro = logradouro;
    }

    public String getBairro() {
        return this.bairro;
    }

    public void setBairro(String bairro) {

        if(bairro == null || bairro.equals("")){
            this.bairro = "-";
        }

        this.bairro = bairro;
    }

    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {

        if(cidade == null || cidade.equals("")){
            this.cidade = "-";
        }

        this.cidade = cidade;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {

        if(estado == null || estado.equals("")){
            this.estado = "-";
        }

        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Cep: " + this.cep +
                "\n"+
                "Tipo de Logradouro: " + this.tipoDeLogradouro +
                "\n"+
                "Logradouro: " + this.logradouro +
                "\n"+
                "Bairro: " + this.bairro +
                "\n"+
                "Cidade: " + this.cidade +
                "\n"+
                "Estado: " + this.estado;
    }
}
