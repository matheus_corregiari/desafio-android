package br.com.matheus.buscacep;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by matheus on 24/09/2014.
 *
 * Interface criada para fazer a requisição REST de busca de Endereco através do cep
 */
@Rest(rootUrl = "http://correiosapi.apphb.com", converters = {MappingJackson2HttpMessageConverter.class})
public interface CepService {

    /**
     * Método que busca um endereco de acordo com o cep
     * @param cep represente o Cep a ser pesquisado
     * @return Endereco de retorno da pesquisa
     *
     */
    @Get("/cep/{cep}")
    Endereco get(String cep);

}
